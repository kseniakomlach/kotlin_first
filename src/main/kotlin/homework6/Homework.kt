package homework6

//write you classes here

//TODO: Homework2 write your function here

fun main() {
    val animalsArray: Array<Animal> = arrayOf(
        Lion("Алекс", 180, 150),
        Tiger("Диего", 230, 230),
        Hippopotamus("Глория", 400, 1500),
        Wolf("Джейкоб", 140, 50),
        Giraffe("Мелман", 6000, 1600),
        Elephant("Флинн", 3500, 4000),
        Chimpanzee("Уошо", 150, 50),
        Gorilla("Коко", 170, 190)
    )
    val foodArray: Array<String> = arrayOf(
        "мясо",
        "мясо",
        "мясо",
        "мясо",
        "трава",
        "трава",
        "трава",
        "трава"
    )
    eat(animalsArray, foodArray)
}

fun eat(animalsArray:Array<Animal>, foodArray: Array<String>){
    if (animalsArray.size == foodArray.size) {
        var i = 0
        while (i in animalsArray.indices) {
            animalsArray[i].eat(foodArray[i])
            i++
        }
    } else println("Количество еды (${foodArray.size}) не совпадает с количеством животных (${animalsArray.size}).")
}
abstract class Animal(open val name: String, open var height: Int, open var weight: Int){
    abstract var foodPreferencesArray:Array<String>
    private var satiety:Int = 0
    fun eat(food: String){
        if (this.foodPreferencesArray.contains(food)) {
            this.satiety += 1
            println("${this.name} съел $food. Сытость - ${this.satiety}.")
        } else println("${this.name} не стал есть $food. Сытость - ${this.satiety}.")
    }
}

class Lion(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("мясо", "зебра", "антилопа гну")
}

class Tiger(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("мясо", "свинья", "косуля", "олень")
}

class Hippopotamus(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("растения", "антилопа", "трава")
}

class Wolf(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("мясо", "лось", "антилопа", "заяц", "лиса")
}

class Giraffe(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("листья", "растения")
}

class Elephant(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("трава", "сено", "яблоки", "морковь")
}

class Chimpanzee(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("фрукты", "растения", "насекомые")
}

class Gorilla(override val name: String, override var height: Int, override var weight: Int):Animal(name, height, weight){
    override var foodPreferencesArray = arrayOf("растения", "орехи", "насекомые")
}
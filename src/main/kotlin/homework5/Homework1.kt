package homework5

fun main() {
    val lion = Lion("Алекс", 180, 150, 7, arrayOf("feed", "birds"))
    val tiger = Tiger("Диего", 230, 230)
    val hippopotamus = Hippopotamus("Глория", 400, 1500, 3)
    val wolf = Wolf("Джейкоб", 140, 50, arrayOf("pizza"))
    val giraffe = Giraffe("Мелман", 6000, 1600)
    val elephant = Elephant("Флинн", 3500, 4000)
    val chimpanzee = Chimpanzee("Уошо", 150, 50)
    val gorilla = Gorilla("Коко", 170, 190, 1)


    lion.eat("meat")
    lion.eat("bread")
    gorilla.eat("nuts")
    gorilla.eat("???")
    println("Сытость ${lion.name} - ${lion.satiety}")
    println("Сытость ${gorilla.name} - ${gorilla.satiety}")

    println("Лев Алекс ест: ")
    for (food in lion.foodPreferencesArray){
        println(food)
    }
}

class Lion(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("meat", "deer", "nilgai");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Tiger(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("meat", "roe", "boar", "elk");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Hippopotamus(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("plant", "seaweed", "grass");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Wolf(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("meat", "elk", "deer");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Giraffe(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("leaf", "plants");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Elephant(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("grass", "hay", "apple", "carrot");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Chimpanzee(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("fruits", "plants", "insects");
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}

class Gorilla(val name: String, var height: Int, var weight: Int){
    var foodPreferencesArray = arrayOf("plants", "nuts", "insects")
    var satiety:Int = 0
    constructor(name: String, height: Int, weight: Int, satiety: Int): this(name, height, weight){
        this.satiety = satiety
    }
    constructor(name: String, height: Int, weight: Int, food: Array<String>): this(name, height, weight){
        this.foodPreferencesArray = food
    }
    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>): this(name, height, weight){
        this.satiety = satiety
        this.foodPreferencesArray = food
    }
    fun eat(food: String){
        if (foodPreferencesArray.contains(food)){
            this.satiety+=1
        }
    }
}
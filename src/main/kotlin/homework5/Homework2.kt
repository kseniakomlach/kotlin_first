package homework5

fun main() {
    val digit = readLine()!!.toInt()
    reverse(digit)
}

fun reverse(digit: Int){
    var value = digit
    while (value>0){
        print(value%10)
        value /= 10
    }
}
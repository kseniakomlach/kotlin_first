package homework2

fun main() {
    val lemonadeMl: Short = 18500
    val cocktailMl: Int = 200
    val whiskeyMl: Byte = 50
    val freshDrops: Long = 3000000000
    val cokeL: Float = 0.5f
    val aleL: Double = 0.666666667
    val nameOfTheDrink: String = "Что-то авторское!"

    println("Заказ - \'$lemonadeMl мл лимонада\' готов!")
    println("Заказ - \'$cocktailMl мл пина колады\' готов!")
    println("Заказ - \'$whiskeyMl мл виски\' готов!")
    println("Заказ - \'$freshDrops капель фреша\' готов!")
    println("Заказ - \'$cokeL литра колы\' готов!")
    println("Заказ - \'$aleL литра эля\' готов!")
    println("Заказ - \'$nameOfTheDrink\' готов!")
}
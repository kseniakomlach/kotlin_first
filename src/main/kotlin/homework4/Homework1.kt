package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var teamCoins = 0
    var joeCoins = 0

    fun isEven(i: Int):Boolean{
        return (i%2 == 0)
    }

    for (coin in myArray){
        if (isEven(coin)) joeCoins++ else teamCoins++
    }

    println("У Джо - $joeCoins монет")
    println("У команды - $teamCoins монет")
}

package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var aStudents = 0
    var bStudents = 0
    var cStudents = 0
    var dStudents = 0
    val countOfMarks = marks.size

    for (mark in marks){
        when (mark){
            5 -> aStudents++
            4 -> bStudents++
            3 -> cStudents++
            2 -> dStudents++
        }
    }

    fun percentageOfStudents(students:Int):Double{
        return ((students.toDouble()/countOfMarks)*100*10).toInt()/10.toDouble()
    }

    println("Отличников - ${percentageOfStudents(aStudents)} процентов")
    println("Хорошистов - ${percentageOfStudents(bStudents)} процентов")
    println("Троечников - ${percentageOfStudents(cStudents)} процентов")
    println("Двоечников - ${percentageOfStudents(dStudents)} процентов")
}
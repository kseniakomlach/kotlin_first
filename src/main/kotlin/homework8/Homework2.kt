package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Количество овощей в корзине: ${countOfVegetablesInUserCart(userCart, vegetableSet)}")
    println("Стоимость всех товаров в корзине: ${priceOfUserCart(userCart, discountSet, discountValue, prices)}")
}

fun countOfVegetablesInUserCart(userCart: Map<String, Int>, vegetableSet: Set<String>): Int{
    var countOfVegetable = 0
    userCart.forEach{
        if (vegetableSet.contains(it.key)){
            countOfVegetable += it.value
        }
    }
    return countOfVegetable
}

fun priceOfUserCart(userCart: Map<String, Int>, discountSet: Set<String>, discountValue: Double, prices: Map<String, Double>): Double{
    var price = 0.0
    userCart.forEach{
        price += if (discountSet.contains(it.key)){
            prices[it.key]!! * it.value * (1-discountValue)
        } else prices[it.key]!! * it.value
    }
    return price
}

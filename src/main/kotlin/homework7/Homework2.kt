package homework7

fun main() {
    try {
        exceptionTest(readLine()!!.toInt())
    } catch (e: CustomException2) {
        println("Это был CustomException2")
    } catch (e: CustomException5) {
        println("Это был CustomException5")
    } catch (e: CustomException1) {
        println("Это был CustomException1")
    } catch (e: CustomException3) {
        println("Это был CustomException3")
    } catch (e: CustomException4) {
        println("Это был CustomException4")
    } catch (e: Exception) {
        println("Это был Exception")
    }
}

open class CustomException1(message: String) : Exception(message)

open class CustomException2(message: String) : CustomException1(message)

open class CustomException3(message: String) : Exception(message)

open class CustomException4(message: String) : Exception(message)

open class CustomException5(message: String) : CustomException4(message)

fun exceptionTest(n: Int) {
    when(n) {
        1 -> throw CustomException1("CustomException1")
        2 -> throw CustomException2("CustomException2")
        3 -> throw CustomException3("CustomException3")
        4 -> throw CustomException4("CustomException4")
        5 -> throw CustomException5("CustomException5")
        else -> throw Exception("Exception")
    }
}
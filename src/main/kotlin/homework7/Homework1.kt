package homework7

fun main() {
    println("Введите логин:")
    val login = readlnOrNull() ?: "NullUser"
    println("Введите пароль:")
    val password = readlnOrNull() ?: "NullPassword"
    println("Введите пароль еще раз:")
    val passwordConfirmation = readlnOrNull() ?: "NullPassword"
    val newUser = createUser(login, password, passwordConfirmation)
    println("Создан новый пользователь: логин - ${newUser.login}, пароль - ${newUser.password}.")
}

fun createUser(login: String, password: String, passwordConfirmation: String):User{
    return if (login != "" && password != "" && passwordConfirmation != ""){
        if (login.length > 20) throw WrongLoginException("Неверный логин: больше 20 символов.")
        if (password.length < 10) throw WrongPasswordException("Неверный пароль: меньше 10 символов.")
        if (password != passwordConfirmation) throw WrongPasswordException("Нужно ввести один и то же пароль дважды.")
        else User(login, password)
    } else User("NullUser", "NullPassword")
}

class WrongLoginException(message: String): Exception(message)
class WrongPasswordException(message: String): Exception(message)

class User(val login: String, val password: String)
